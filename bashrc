#!/bin/sh
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $SOURCE == /* ]]; then
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
RDIR="$( dirname "$SOURCE" )"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
export RAD_DIR=$DIR
export PATH=$RAD_DIR/install/bin:$PATH

# Configure useful aliases
alias rad='cd $RAD_DIR'
alias tuto='cd $RAD_DIR/tutorials'
alias src='cd $RAD_DIR/src/pureRadiation'
alias mod='module purge; module use -a /nasa/modulefiles/testing; module load comp-pgi/19.5; module load mpi-hpe; module load cuda/10.1'
