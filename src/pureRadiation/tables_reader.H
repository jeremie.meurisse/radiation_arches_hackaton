// Read the geometry, radiation and field tables
std::ifstream in_normal_faces("tables/normal_faces");
std::ifstream in_elem_neighbor("tables/elem_neighbor");
std::ifstream in_advance_order("tables/advance_order");
std::ifstream in_elem_volume("tables/elem_volume");
std::ifstream in_n_elem("tables/n_elem");
std::ifstream in_n_dir("tables/n_dir");
std::ifstream in_bin_table("tables/bin");
std::ifstream in_pressure_table("tables/pressure");
std::ifstream in_temperature_table("tables/temperature");

int normal_faces_size;
in_normal_faces >> normal_faces_size;
//std::vector<int> normal_faces_sizes(normal_faces_size);
int * normal_faces_sizes = new int [normal_faces_size];
for (int i=0; i< normal_faces_size; i++)
{
  in_normal_faces >> normal_faces_sizes[i];
}

//std::vector<std::vector<std::vector<double> > > normal_faces(normal_faces_size); 
array3d<double> normal_faces(normal_faces_size, normal_faces_sizes[0], 3); // normal_faces[][][]
//std::vector<std::vector<std::vector<double> > > normal_faces(normal_faces_size);
//normal_faces = new double * * [normal_faces_size];
for (int i=0; i< normal_faces_size; i++)
{
  //  normal_faces[i].resize(normal_faces_sizes[i]);
  //normal_faces[i] = new double * [normal_faces_sizes[i]];
  for (int j=0; j< normal_faces_sizes[i]; j++) {
    //    normal_faces[i][j].resize(3);
    //normal_faces[i][j] = new double [3];
    for (int k=0; k<3; k++) {
      in_normal_faces >> normal_faces(i,j,k);
    }
  }
}


int elem_neighbor_size;
in_elem_neighbor >> elem_neighbor_size;
//std::vector<int> elem_neighbor_sizes(elem_neighbor_size);
int * elem_neighbor_sizes = new int [elem_neighbor_size];
for (int i=0; i< elem_neighbor_size; i++)
{
  in_elem_neighbor >> elem_neighbor_sizes[i];
}

//double ** elem_neighbor;
array2d<double> elem_neighbor(elem_neighbor_size, elem_neighbor_sizes[0]);
//elem_neighbor = new double*[elem_neighbor_size];
//std::vector<std::vector<double> > elem_neighbor(elem_neighbor_size);
for (int i=0; i< elem_neighbor_size; i++)
{
  //elem_neighbor[i].resize(elem_neighbor_sizes[i]);
  //elem_neighbor[i] = new double[elem_neighbor_sizes[i]];
  for (int j=0; j< elem_neighbor_sizes[i]; j++) {
    in_elem_neighbor >> elem_neighbor(i,j);
  }
}


int advance_order_size;
in_advance_order >> advance_order_size;
//std::vector<int> advance_order_sizes(advance_order_size);
int * advance_order_sizes = new int[advance_order_size];
//array2d<int> advance_order_sizes(advance_order_size, );

for (int i=0; i< advance_order_size; i++)
{
  in_advance_order >> advance_order_sizes[i];
}

//std::vector<std::vector<int> > advance_order(advance_order_size);
//int ** advance_order;
//advance_order = new int*[advance_order_size];
array2d<int> advance_order(advance_order_size, advance_order_sizes[0]);

for (int i=0; i< advance_order_size; i++)
{
  //advance_order[i].resize(advance_order_sizes[i]);
  //advance_order[i] = new int[advance_order_sizes[i]];
  for (int j=0; j< advance_order_sizes[i]; j++) {
    in_advance_order >> advance_order(i,j);
  }
}


int elem_volume_size;
in_elem_volume >> elem_volume_size;
//std::vector<double> elem_volume(elem_volume_size);
double * elem_volume = new double[elem_volume_size];
for (int i=0; i< elem_volume_size; i++)
{
  in_elem_volume >> elem_volume[i];
}

int n_elem;
in_n_elem >> n_elem;


int n_Dir_;
in_n_dir >> n_Dir_;


int nBins;
int nPress;
int nTemp;
int bin_table_size;
in_bin_table >> nBins;
in_bin_table >> nPress;
in_bin_table >> nTemp;
in_bin_table >> bin_table_size;
//std::vector<double> bin_table(bin_table_size);
double * bin_table = new double [bin_table_size];
for (int i=0; i<bin_table_size; i++)
{
  in_bin_table >> bin_table[i];
}

int size_pressure_;
int size_temperature_;
in_pressure_table >> size_pressure_;
in_temperature_table >> size_temperature_;
//std::vector<double> pressure(size_pressure_);
//std::vector<double> temperature(size_temperature_);
double * pressure = new double[size_pressure_];
double * temperature = new double[size_pressure_];
for (int i = 0; i<size_pressure_; i++)
{
  in_pressure_table >> pressure[i];
}
for (int i = 0; i<size_temperature_; i++)
{
  in_temperature_table >> temperature[i];
}
