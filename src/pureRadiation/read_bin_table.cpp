
#include "read_bin_table.H"

// Read opacity and source data from binary tables
// and initialize arrays
void table_data_bin::read_table(double * table_, int& nB, int& nP, int& nT)
{
  nBins=nB;
  nPress=nP;
  nTemp=nT;
  
  //Pressures.resize(nPress);                                                                                    
  //Temperatures.resize(nTemp);                                                                                  
  if(Pressures!=NULL){
      delete [] Pressures;
  }
  Pressures = new double[nPress];
  
  if(Temperatures!=NULL){
      delete [] Temperatures;
  }
  Temperatures = new double[nTemp];
                                                                                                               
  int index=0;                                                                                                      
  for (int p = 0 ; p < nPress ; p++ )                                                                          
    {                                                                                                          
      Pressures[p]=table_[index] ;  
      index++;
    }                                                                                                          
                                                                                                               
  for (int t = 0 ; t < nTemp ; t++ )                                                                           
    {        
      Temperatures[t]=table_[index] ;                                                                    
      index++;
    }                                                                                                          

  if(m_opacity!=NULL){
    delete [] m_opacity;
  }
  m_opacity = new double[nPress*nBins*nTemp];
    
  if(m_source!=NULL){
    delete [] m_source;
  }
  m_source = new double[nPress*nBins*nTemp];                                                                                                         
  //m_opacity.resize(nPress*nBins*nTemp);                                                                        
  //m_source.resize(nPress*nBins*nTemp);                                                                         
  
  for (int n = 0 ; n < nBins*nPress*nTemp ; n++ )                                                         
    {                                                                                                          
                                                             
      m_opacity[n]= table_[index] ;
      index++;
      m_source[n]= table_[index] ; 
      index++;
    }           

  std::cout << "nBin " << nBins << " nTemp " << nTemp << " nPress " << nPress << std::endl;

}

// Compute values based on coordinates using linear interpolation
void table_data_bin::calc_value( 
                     const int& n_cell,  
                     double* P,
                     double* T,
                     double* source,
                     double* opacity)
{
  
  //int n_cell=P.size();
  //int n_cell_T = T.size();
  //if(n_cell == n_cell_T){
//    source.resize(n_cell);
//    opacity.resize(n_cell);
  //}
  //else{
  //  std::cout << "Error during source,opacity calculation: n_cell T != n_cell p" << std::endl;
    
  //}


  for(int cellI =0 ; cellI<n_cell ; cellI++)
    {
  
      double P0 = 101325;
      double pressure=P[cellI]/P0;
      double temp_cell =  T[cellI];  

      // Minimum p,T
      if( pressure < 1 ){
	pressure=1+1e-10;
      }
      if( temp_cell < 300 ){
	temp_cell=300+1e-10;
      }
 
    
      if      (pressure<Pressures[0]) pressure = Pressures[0];
      else if (pressure>Pressures[nPress-1])  pressure = Pressures[nPress-1];

      if      (temp_cell<Temperatures[0]) temp_cell = Temperatures[0];
      else if (temp_cell>Temperatures[nTemp-1])  temp_cell = Temperatures[nTemp-1];
       
      // lookup the requested value and return a linear interpolation
      //std::vector<double>::iterator T_upper_it =
	      //std::lower_bound(Temperatures.begin(),Temperatures.end(),temp_cell);

      //assert (*T_upper_it >= temp_cell);
      
      unsigned int idT = 0; //std::distance (Temperatures.begin(), T_upper_it) - 1;
      for(unsigned int it=0; it<(nTemp-1); it++){
          if( (Temperatures[it]<temp_cell) && (Temperatures[it+1]>=temp_cell) ){
              idT=it;
              break;
          }
      }

      assert (idT+1 < nTemp);
      assert (Temperatures[idT]   <= temp_cell);
      assert (Temperatures[idT+1] >= temp_cell);


      // Check for pressure
      //std::vector<double>::iterator P_upper_it =
	  //    std::lower_bound(Pressures.begin(),
	  //		 Pressures.end(),
	  //		 pressure);
      
      unsigned int idP = 0; //std::distance (Temperatures.begin(), T_upper_it) - 1;
      for(unsigned int ip=0; ip<(nPress-1); ip++){
          if( (Pressures[ip]<pressure) && (Pressures[ip+1]>=pressure) ){
              idP=ip;
              break;
          }
      }
      
      //const unsigned int idP = std::distance (Pressures.begin(), P_upper_it) - 1;
      
      assert (idP+1 < nPress);
      assert (Pressures[idP]   <= pressure);
      assert (Pressures[idP+1] >= pressure);

            
      // linear interpolation between specified values
      double opacity_l, opacity_u;
      double source_l, source_u;
      
      double percent = (temp_cell - Temperatures[idT]) / (Temperatures[idT+1] - Temperatures[idT]);
	     
      // P_lower
      opacity_l = m_opacity[idT + bin*nTemp + idP*nBins*nTemp] + percent*(m_opacity[(idT+1) + bin*nTemp + idP*nBins*nTemp] - m_opacity[idT + bin*nTemp + idP*nBins*nTemp]);
      source_l  = m_source[idT + bin*nTemp + idP*nBins*nTemp]
	+ percent*(m_source[(idT+1) + bin*nTemp + idP*nBins*nTemp] - m_source[idT + bin*nTemp + idP*nBins*nTemp]);

      // P_upper
      opacity_u = m_opacity[idT + bin*nTemp + (idP+1)*nBins*nTemp]
	+ percent*(m_opacity[(idT+1) + bin*nTemp + (idP+1)*nBins*nTemp] - m_opacity[idT + bin*nTemp + (idP+1)*nBins*nTemp]);
      source_u  = m_source[idT + bin*nTemp + (idP+1)*nBins*nTemp]
	+ percent*(m_source[(idT+1) + bin*nTemp + (idP+1)*nBins*nTemp] - m_source[idT + bin*nTemp + (idP+1)*nBins*nTemp]);

     
      opacity[cellI] = opacity_l + (pressure - Pressures[idP])/(Pressures[idP+1]-Pressures[idP])*(opacity_u - opacity_l);
      source[cellI]  = source_l + (pressure - Pressures[idP])/(Pressures[idP+1]-Pressures[idP])*(source_u - source_l);    
      
      // Convert to SI units
      opacity[cellI] *=1.e2; // [1/m]
      source[cellI]  *= 1.e6; // [W/m^3] 
    }
}

